package uy.com.bqm.keycloak.auth.provider.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor 
public class Usuario implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
 

    private DatosContacto datosContacto;

    private DatosPersonales datosPersonales;

    private DatosCredenciales datosCredenciales;
 

}


