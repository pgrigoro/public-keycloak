package uy.com.bqm.keycloak.auth.provider.model;

import java.io.Serializable;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor 
public class DatosPersonales implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String nombres;

	private String apellidos;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate fechaNacimiento;

	private String numeroDocumento;
 
	private String serie;

	private int numeroFolio;
 
}
