package uy.com.bqm.keycloak.auth.provider;

import org.springframework.stereotype.Service;

import uy.com.bqm.keycloak.auth.client.BqmAuthException;
import uy.com.bqm.keycloak.auth.client.RestTemplateUsuarioClient;
import uy.com.bqm.keycloak.auth.provider.model.Usuario;

@Service
public class BqmUserRepository {

	RestTemplateUsuarioClient client = RestTemplateUsuarioClient.getInstance();

	public boolean validateCredentials(String username, String password) throws BqmAuthException {

		Usuario usuario = client.getUserByUsernameAndPassword(username, password);
		return usuario != null;

	}

	public BqmUser findBqmUserByUsername(String username) {

		Usuario usuario = client.getUserByUsername(username);

		BqmUser user = new BqmUser(usuario.getDatosCredenciales().getCuentaId(),
				usuario.getDatosCredenciales().getUsuario());
		user.setEmail(usuario.getDatosContacto().getEmail());
		user.setFirstName(usuario.getDatosPersonales().getNombres());
		user.setLastName(String.valueOf(usuario.getDatosCredenciales().getCuentaId()));
		user.setAccountId(usuario.getDatosCredenciales().getCuentaId());
		return user;
	}

}
