package uy.com.bqm.keycloak.auth.provider;

import org.keycloak.component.ComponentModel;
import org.keycloak.credential.CredentialInput;
import org.keycloak.credential.CredentialInputValidator;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.storage.StorageId;
import org.keycloak.storage.UserStorageProvider;
import org.keycloak.storage.user.UserLookupProvider;

public class BqmUserStorageProvider implements UserStorageProvider, UserLookupProvider, CredentialInputValidator {

	private final KeycloakSession session;
	private final ComponentModel model;
	private final BqmUserRepository repository;

	public BqmUserStorageProvider(KeycloakSession session, ComponentModel model, BqmUserRepository repository) {
		this.session = session;
		this.model = model;
		this.repository = repository;
	}

	@Override
	public void close() {

	} 

	@Override
	public boolean isValid(RealmModel realm, UserModel user, CredentialInput credentialInput) {
		
		return this.repository.validateCredentials(user.getUsername(), credentialInput.getChallengeResponse());
	}
	
	@Override
	public UserModel getUserByUsername(String username, RealmModel realm) {

		return new BqmUserAdapter(session, realm, model, repository.findBqmUserByUsername(username));

	}

	@Override
	public UserModel getUserByEmail(String email, RealmModel realm) {
		return null;
	}

	@Override
	public boolean supportsCredentialType(String credentialType) {
		return true;
	}

	@Override
	public boolean isConfiguredFor(RealmModel realm, UserModel user, String credentialType) {
		return true;
	}

	@Override
	public UserModel getUserById(String id, RealmModel realm) {
		StorageId storageId = new StorageId(id);
        String username = storageId.getExternalId();
        return getUserByUsername("CHINO20", realm);
	}


}
