package uy.com.bqm.keycloak.auth.provider.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Builder
public class DatosContacto implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String celular;

    private String email;
    
    private String departamentoResidencia;

    private boolean apostarPorSms;

    private boolean recibeEmail;

    }
