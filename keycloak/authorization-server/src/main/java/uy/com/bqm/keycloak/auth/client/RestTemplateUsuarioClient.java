package uy.com.bqm.keycloak.auth.client;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.core.Response.Status;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import uy.com.bqm.keycloak.auth.provider.model.Login;
import uy.com.bqm.keycloak.auth.provider.model.Usuario;

public class RestTemplateUsuarioClient {

	private static RestTemplateUsuarioClient INSTANCE = null;

	private static Logger log = LoggerFactory.getLogger(RestTemplateUsuarioClient.class);

	private RestTemplate restTemplate;

	private String endpoint;

	private String proxyHost;

	private String proxyUser;

	private String proxyPass;

	private int proxyPort;

	private boolean proxyEnabled;

	private String pathUsers;

	private String pathLogin;

	private String genericError;

	private static final String ERROR_TYPE = "Error";

	private RestTemplateUsuarioClient() {
		try {
			loadConfig();
			restTemplate = getRestTemplate();
		} catch (IOException e) {
			throw new BqmAuthException(ERROR_TYPE, genericError, Status.FORBIDDEN);
		}
	}

	public static RestTemplateUsuarioClient getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new RestTemplateUsuarioClient();
		}
		return INSTANCE;
	}

	public Usuario getUserByUsername(String username) {
		try {

			Map<String, String> urlParams = new HashMap<>();
			String url = RestCallEndpointBuilder.builderWhitOnlyPathParams(endpoint + pathUsers + username, urlParams);
			HttpEntity<?> httpEntity = new HttpEntity<>(RestCallEndpointBuilder.getDefaultHttpHeaders());
			ResponseEntity<Usuario> response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Usuario.class);

			return response.getBody();

		} catch (HttpClientErrorException e) {
			log.error("HttpClientErrorException!!", e);
			throw new BqmAuthException(ERROR_TYPE, "Usuario y/o clave incorrectos", Status.FORBIDDEN);
		} catch (Exception e) {
			log.error("Error generico!!", e);
			throw new BqmAuthException(ERROR_TYPE, genericError, Status.FORBIDDEN);
		}

	}

	public Usuario getUserByUsernameAndPassword(String username, String password) throws BqmAuthException {

		try {

			Map<String, String> urlParams = new HashMap<>();
			String url = RestCallEndpointBuilder.builderWhitOnlyPathParams(endpoint + pathLogin, urlParams);

			Login login = new Login();
			login.setPassword(password);
			login.setUser(username);

			HttpEntity<Login> httpEntity = new HttpEntity<>(login, RestCallEndpointBuilder.getDefaultHttpHeaders());
			ResponseEntity<Usuario> response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, Usuario.class);

			return response.getBody();

		} catch (HttpClientErrorException e) {
			log.error("HttpClientErrorException!!", e);
			throw new BqmAuthException(ERROR_TYPE, e.getResponseBodyAsString(), Status.FORBIDDEN);
		} catch (Exception e) {
			log.error("Error generico!!", e);
			throw new BqmAuthException(ERROR_TYPE, genericError, Status.FORBIDDEN);
		}

	}

	public RestTemplate getRestTemplate() {
		RestTemplate template = new RestTemplate();

		HttpClientBuilder clientBuilder = HttpClientBuilder.create();

		HttpClient httpClient = clientBuilder.build();
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.setHttpClient(httpClient);

		template.setRequestFactory(factory);

		if (proxyEnabled) {

			CredentialsProvider credsProvider = new BasicCredentialsProvider();
			credsProvider.setCredentials(new AuthScope(proxyHost, proxyPort),
					new UsernamePasswordCredentials(proxyUser, proxyPass));

			HttpHost myProxy = new HttpHost(proxyHost, proxyPort);
			clientBuilder.setProxy(myProxy).setDefaultCredentialsProvider(credsProvider).disableCookieManagement();

			httpClient = clientBuilder.build();
			factory = new HttpComponentsClientHttpRequestFactory();
			factory.setHttpClient(httpClient);

			template.setRequestFactory(factory);

		}

		return template;
	}

	private void loadConfig() throws IOException {

		InputStream input = RestTemplateUsuarioClient.class.getClassLoader()
				.getResourceAsStream("bqmbroker.properties");

		Properties prop = new Properties();
		prop.load(input);

		endpoint = prop.getProperty("endpoint");
		genericError = prop.getProperty("generic-error");
		pathLogin = prop.getProperty("path-login");
		pathUsers = prop.getProperty("path-users");
		proxyEnabled = Boolean.valueOf(prop.getProperty("proxy-enabled"));
		proxyHost = prop.getProperty("proxy-host");
		proxyPort = Integer.parseInt(prop.getProperty("proxy-port"));
		proxyPass = prop.getProperty("proxy-pass");
		proxyUser = prop.getProperty("proxy-user");

	}

}