package uy.com.bqm.keycloak.auth.provider.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor 
public class DatosCredenciales implements Serializable {

	private Long cuentaId;

    private String usuario;

    private String password;
 
}
