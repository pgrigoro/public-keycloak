package uy.com.bqm.keycloak.auth.provider;

import java.util.List;

import org.keycloak.Config;
import org.keycloak.component.ComponentModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.provider.ProviderConfigurationBuilder;
import org.keycloak.storage.UserStorageProviderFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.google.auto.service.AutoService;

@AutoService(UserStorageProviderFactory.class)
public class BqmUserStorageProviderFactory
		implements UserStorageProviderFactory<BqmUserStorageProvider> {

	private static final String PROVIDER_ID = "bqm-user-provider";


	@Override
	public void init(Config.Scope config) {

	}

	@Override
	public String getId() {
		return PROVIDER_ID;
	}

	@Override
	public BqmUserStorageProvider create(KeycloakSession session, ComponentModel model) {

		BqmUserRepository repository = new BqmUserRepository();

		return new BqmUserStorageProvider(session, model, repository);
	}

	@Override
	public List<ProviderConfigProperty> getConfigProperties() {

		// this configuration is configurable in the admin-console
		return ProviderConfigurationBuilder.create().property().name("myParam").label("My Param")
				.helpText("Some Description").type(ProviderConfigProperty.STRING_TYPE).defaultValue("some value").add()
				// more properties
				// .property()
				// .add()
				.build();
	}

}